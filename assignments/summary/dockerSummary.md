# Docker Basics
It becomes hard to manage dependencies as we build large projects and if we want to deploy them in various environments. So we will be using docker to deploy our product.

# Essential terms to understand Docker

| Terms | Meaning |
| ------ | ------ |
| Docker | program to develop and run applications with containers |
| Docker Image | contains code, runtime, libraries, environmental variables, configuration files to run applications |
| Container | running Docker Image |
| Docker Hub | like GitHub but for docker images and containers |

#Getting started with Docker
## Docker Installation

For Ubuntu, refer the image below-
![ubuntu](docker1.png)

## Docker Commands

| Command | Meaning |
| ------ | ------ |
| docker ps | allows to view all the containers that are running on the Docker Host |
| docker start | starts any stopped containers |
| docker stop | stops any running containers |
| docker run | creates containers from docker images |
| docker rm | deletes the containers |


## Docker Operations

1. Download/pull the docker images that you want to work with.
2. Copy your code inside the docker
3. Access docker terminal
4. Install and additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run your program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try your code

See more [ Docker Command line](https://docs.docker.com/engine/reference/commandline/cli/)
