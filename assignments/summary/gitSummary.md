# Introduction to Git
Git is used to manage code.It is version-control software that makes collaboration simple and lets you easily keep track of every revision you and your team make during the development of your software.

# Essential terms to Understand Git

| Term | Meaning |
| ------ | ------ |
| Repository | collection of files and folders |
| Gitlab | mote storage solution for git repos |
|Commit | saving work |
|Push | syncing your commits |
|Branch | separate instances of code |
|Merge | integrating two branches together |
|Clone | making exact copy of repo |
| Fork | getting entirely new repo of repo under your own name |

# Getting Started with Git
## Installing Git on your system
For Linux, open the terminal and type the following command- sudo apt-get install git
or [download the package](https://git-scm.com/download/linux)
![Linux Terminal](git5.png)
![LInux Terminal](git6.png)

For windows, [download the installer](https://git-scm.com/download/win)

## Git Workflow
![git workflow](git7.png)

